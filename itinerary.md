Booking.com: use kortingslink:
------
    - https://www.booking.com/s/18_6/bcfaf0af - van samuel voor melissa
    - https://www.booking.com/s/18_6/d2bb81c4 - melissa voor sam

Days:
-------
    - Kumano kodo: http://www.tb-kumano.jp/en/kumano-kodo/suggested-walks/#takijiri-hongu
        - luggage shuttle: http://www.kuronekoyamato.co.jp/ytc/en/send/preparations/invoice/

Nights:
--------
    - *Tokyo: 2 BOOKED*
        - https://www.booking.com/hotel/jp/apa-hotel-nihombashi-eki-kita.en-gb.html
        - Samuel's booking account
        - cancellable untill 13-10
        - price € 249 ¥32,000 + 2x15 cashback

    - *Kyoto: 3 BOOKED*
        - €346
        - melissa's booking account
        -

    - *Tanabe 1 BOOKED*
        - https://www.booking.com/hotel/jp/the-cue.en-gb.html - nicer pictures, possibly no shared bathroom
        - Samuel's Booking Account
        - cancellable untill 12/10
        - booked 20/10-21/10,
        - 10500 yen = 82 eur + 15euro cashback

    - *Nahahecki area: 1 BOOKED*
        - via kii-tanabe travel agency
        - 32500Y = 280 euro
        - voucher in this repo:
        - https://www.kumano-travel.com/index.php?action_MyPage_RequestList=true
        - emails tagged kumano kodo

    - *Hongu area: 1 BOOKED*
        - https://www.booking.com/hotel/jp/yunominesou.nl.html
        - samuel booking
        - cancellable untill 17-10
        - 177euro

    - *Koyasan: 1 BOOKED*
        - https://www.booking.com/hotel/jp/sekishoin.nl.html
        - Samuels booking
        - cancellable until 15-10
        - 223euro + 15 cashback

    - *Nara: 1 BOOKED*
        - https://www.booking.com/hotel/jp/family-inn-nara.nl.html
        - Melissa's Booking Account
        - cancellable untill 16/10
        - booked 24/10-25/10,
        - 10800 yen = 85 eur + 15euro cashback

    - *Hiroshima: 2 BOOKED*
        - https://www.booking.com/hotel/jp/hondori-inn.en-gb.html
        - melissa booking
        - 50% refundable + 50% paid by samuel
        - 244 euro + 15eu cashback

    - *Hakone/tokyo: 1 BOOKED*
        - https://www.booking.com/hotel/jp/hotel-daihakone.en-gb.html
        - samuel booking
        - 218 eu + 30 eu cashback

    - Tokyo: 2
        - https://www.booking.com/hotel/jp/book-and-bed-tokyo-shinjuku.en-gb.html
        - samuel booking
        - 247 eu + 30 cashback
        - cancellable untill 21-10

Itinerary including Tokyo, Kyoto, Tanabe, Hiroshima, Nara, Hongu (area), Kumona Kodo Hike, Hakone, Koya-san, Nakahechi (area).

Arrival
-------
15/10 07:50 Tokyo.

15/10
-----

day: full day tokyo (ginza + asakusa) [day 2](https://nerdnomads.com/what-to-do-in-tokyo),

night: dinner + sleep in tokyo https://www.booking.com/hotel/jp/apa-hotel-nihombashi-eki-kita.en-gb.html
16/10
------

day: full day tokyo (asakusa, ueno) [day3](https://nerdnomads.com/what-to-do-in-tokyo)

night: dinner + sleep in tokyo https://www.booking.com/hotel/jp/apa-hotel-nihombashi-eki-kita.en-gb.html

17/10
------

day: part day tokyo, travel to kyoto 3,5 hrs

night: dinner + sleep in kyoto

18/10
-------

day: full day kyoto

night: sleep in kyoto

19/10
-------

day: full day kyoto

night: sleep in kyoto

20/10
------

day: part day kyoto + travel to tanabe 3,5hrs

night: dinner + sleep in tanabe https://www.booking.com/hotel/jp/the-cue.en-gb.html (Samuel's Booking Account)

21/10
------

day: bus to takijiri 30mins + walk kumano kodo to nakahechi area 6hrs (food)

night: dinner arranged + sleep chikatsuyu minshuku - reservation confirmed, voucher in repo:

22/10
------

day: walk kumano kodo to hongu area 8hrs (food for breakfast and lunch arranged)

night: dinner + sleep hongu area https://www.booking.com/hotel/jp/yunominesou.nl.html (samuel booking)

23/10
-----

day: travel to gojo 4hrs + travel to koyasan 1,5hrs + koyasan part day

night: dinner + sleep koyasan https://www.booking.com/hotel/jp/sekishoin.nl.html (Samuel's booking account)

24/10
-----

day: travel to nara 3,5hrs + nara part day

night: dinner + sleep nara https://www.booking.com/hotel/jp/family-inn-nara.nl.html (melissa's booking account)

25/10
-------

day: travel to hiroshima 3hrs + part day hiroshima

night: dinner + sleep hiroshima https://www.booking.com/hotel/jp/hondori-inn.en-gb.html (melissa booking)

26/10
--------

day: full day hiroshima (miyajima island?)

night: dinner + sleep hiroshima https://www.booking.com/hotel/jp/hondori-inn.en-gb.html (melissa booking)

27/10
------

day: travel to hakone 5hrs + part day hakone
night: dinner + sleep hakone https://www.booking.com/hotel/jp/hotel-daihakone.en-gb.html (samuel booking)

28/10
------

day: travel to tokyo 1h + near-full day tokyo (shinjuku) [day 4](https://nerdnomads.com/what-to-do-in-tokyo)
night: dinner + sleep tokyo https://www.booking.com/hotel/jp/book-and-bed-tokyo-shinjuku.en-gb.html

29/10
------

day: full day tokyo (harajuku/omotesando/shibuya) [day 1](https://nerdnomads.com/what-to-do-in-tokyo)
night: dinner + sleep tokyo https://www.booking.com/hotel/jp/book-and-bed-tokyo-shinjuku.en-gb.html

30/10
------

day:
    - go to airport at 09:00
    - Fly at 12:45 japan time.

night: die in nederland
